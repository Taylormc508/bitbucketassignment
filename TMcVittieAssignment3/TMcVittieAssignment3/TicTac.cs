﻿/*
 * Created by: Taylor McVittie
 * 
 * 
 * Created date: November 19 2019
 * 
 * 
 * Created for: Tic Tac Toe
 * 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TMcVittieAssignment3
{

    public partial class TicTac : Form
    {
        string[,] xyArray = new string[3, 3];
        string nameX = "";
        string nameO = "";
        bool whosTurn = true;
        bool pickXOrO = true;
        bool playerName = false;

        public TicTac()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This is form load and will make it show whos turn it is on screen for the begining of the game.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TicTac_Load(object sender, EventArgs e)
        {
            turn();
        }
        /// <summary>
        /// This is the first Picture box in the first row from the top.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb1_Click(object sender, EventArgs e)
        {
            playerNames();

            if (pickXOrO == true && xyArray[0, 0] == null)
            {
                picb1.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[0, 0] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[0, 0] == null)
            {
                picb1.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[0, 0] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();

        }
        /// <summary>
        /// This is the second picture box in the frist row from top.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb2_Click(object sender, EventArgs e)
        {
            playerNames();
            if (pickXOrO == true && xyArray[0, 1] == null)
            {
                picb2.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[0, 1] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[0, 1] == null)
            {
                picb2.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[0, 1] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the third picture box in the first row from the top.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb3_Click(object sender, EventArgs e)
        {
            playerNames();
            if (pickXOrO == true && xyArray[0, 2] == null)
            {
                picb3.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[0, 2] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[0, 2] == null)
            {
                picb3.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[0, 2] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the first picture box in the first row in the middle row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb4_Click(object sender, EventArgs e)
        {
            playerNames();
            if (pickXOrO == true && xyArray[1, 0] == null)
            {
                picb4.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[1, 0] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[1, 0] == null)
            {
                picb4.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[1, 0] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the second picture box in the middle row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb5_Click(object sender, EventArgs e)
        {
            playerNames();
            if (pickXOrO == true && xyArray[1, 1] == null)
            {
                picb5.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[1, 1] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[1, 1] == null)
            {
                picb5.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[1, 1] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the third picture box in the middle row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb6_Click(object sender, EventArgs e)
        {
            playerNames();
            if (pickXOrO == true && xyArray[1, 2] == null)
            {
                picb6.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[1, 2] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[1, 2] == null)
            {
                picb6.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[1, 2] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the first picture box in the third row from the top.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb7_Click(object sender, EventArgs e)
        {
            playerNames();
            if (pickXOrO == true && xyArray[2, 0] == null)
            {
                picb7.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[2, 0] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[2, 0] == null)
            {
                picb7.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[2, 0] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the second picture box in the third row from the top.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb8_Click(object sender, EventArgs e)
        {
            playerNames();
            if (pickXOrO == true && xyArray[2, 1] == null)
            {
                picb8.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[2, 1] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[2, 1] == null)
            {
                picb8.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[2, 1] = "O";
                turn();
            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the third picture box in the third row from the top.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picb9_Click(object sender, EventArgs e)
        {
            
            playerNames();
            if (pickXOrO == true && xyArray[2, 2] == null)
            {
                picb9.Image = Properties.Resources.RedX;
                pickXOrO = false;
                xyArray[2, 2] = "X";
                turn();
            }
            else if (pickXOrO == false && xyArray[2, 2] == null)
            {
                picb9.Image = Properties.Resources.redO;
                pickXOrO = true;
                xyArray[2, 2] = "O";
                turn();

            }
            else
            {
                MessageBox.Show("Please select an empty box");
            }
            check();
        }
        /// <summary>
        /// This is the submit button on the form for if the user wants to add names to the game.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            playerNames();
            nameX = txtXPlayer.Text;
            nameX = nameX.ToUpper();
            lblWhosTurn.Text = nameX + " Turn";

        }
        /// <summary>
        /// This is a method to check for a winner.
        /// </summary>
        void check()
        {
            /*This if is to check for a tie. it makes sure that evey spot in the array is = to x or o and that it is not 
             = to null. If it is true than it will show Game is a Tie in a message box and call for Endgame Method.*/
            if (xyArray[0, 0] != null && xyArray[0, 1] != null && xyArray[0, 2] != null &&
                xyArray[1, 0] != null && xyArray[1, 1] != null && xyArray[1, 2] != null &&
                xyArray[2, 0] != null && xyArray[2, 1] != null && xyArray[2, 2] != null)
            {
                lblWhosTurn.Text = "";
                MessageBox.Show("Game is a Tie");
                Endgame();
            }
            /*These for statements are for checking for a winner for X or O going across the rows of 1, 2 or 3 for 
             both X and O And also going down in colume 1,2 or 3.*/
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (xyArray[i, 0] == "X" && xyArray[i, 1] == "X" && xyArray[i, 2] == "X")
                    {
                        if (playerName == true)
                        {
                            MessageBox.Show(nameX + " Wins!");
                            Endgame();
                            i = 3;
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Winner Player X");
                            Endgame();
                            i = 3;
                            break;
                        }
                      
                    }
                    else if (xyArray[i, 0] == "O" && xyArray[i, 1] == "O" && xyArray[i, 2] == "O")
                    {
                        if (playerName == true)
                        {
                            MessageBox.Show(nameO + " Wins!");
                            Endgame();
                            i = 3;
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Winner Player O");
                            Endgame();
                            i = 3;
                            break;
                        }
                       
                    }
                    else if (xyArray[0, i] == "X" && xyArray[1, i] == "X" && xyArray[2, i] == "X")
                    {
                        if (playerName == true)
                        {
                            MessageBox.Show(nameX + " Win's!");
                            Endgame();
                            i = 3;
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Winner Player X");
                            Endgame();
                            i = 3;
                            break;
                        }
                    }
                    else if (xyArray[0, i] == "O" && xyArray[1, i] == "O" && xyArray[2, i] == "O")
                    {
                        if (playerName == true)
                        {
                            MessageBox.Show(nameO + " Win's!");
                            Endgame();
                            i = 3;
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Winner Player O");
                            Endgame();
                            i = 3;
                            break;
                        }
                    }
                }
            }
            // This array is to check if there is a winner for going sideways in the tic tac toe game.
            if (xyArray[0, 0] == "X" && xyArray[1, 1] == "X" && xyArray[2, 2] == "X")
            {
                if (playerName == true)
                {
                    MessageBox.Show(nameX + " Win's");
                    Endgame();
                }
                else
                {
                    MessageBox.Show("Winner Player X");
                    Endgame();
                }
                
            }
            else if (xyArray[0, 0] == "O" && xyArray[1, 1] == "O" && xyArray[2, 2] == "O")
            {
                if (playerName == true)
                {
                    MessageBox.Show(nameO + " Win's");
                    Endgame();
                }
                else
                {
                    MessageBox.Show("Winner Player O");
                    Endgame();
                }
            }

            if (xyArray[2, 0] == "X" && xyArray[1, 1] == "X" && xyArray[0, 2] == "X")
            {
                if (playerName == true)
                {
                    MessageBox.Show(nameX + " Win's");
                    Endgame();
                }
                else
                {
                    MessageBox.Show("Winner Player X");
                    Endgame();
                }
            }
            else if (xyArray[2, 0] == "O" && xyArray[1, 1] == "O" && xyArray[0, 2] == "O")
            {
                if (playerName == true)
                {
                    MessageBox.Show(nameO + " Win's");
                    Endgame();
                }
                else
                {
                    MessageBox.Show("Winner Player O");
                    Endgame();
                }
            }
        }
        /// <summary>
        /// This is a method for the end of the game to reset the game board and also reset the names.
        /// </summary>
        void Endgame()
        {
            /*These two for statements are to make every spot in the array = null.*/
            for (int i = 0; i <= 2; i++)
            {
                
                for (int j = 0; j <= 2; j++)
                {
                    xyArray[i, j] = null;
                }
            }
            /*This is to make every picture box on the game board to = null meaning the images will disappear.*/
            foreach (PictureBox pictureBox in pnlGameBoard.Controls)
            {
                pictureBox.Image = null;
            }
            /*This reset the bulls back to what they were at the begining of the game and also restes the text boxs to empty.*/
            whosTurn = true;
            pickXOrO = true;
            playerName = false;
            txtXPlayer.Text = "";
            txtOPlayer.Text = "";
        }
        /// <summary>
        /// This is to check whos turn it is in the game.
        /// </summary>
        void turn()
        {
            /*This is to check whos turn it is. This if stament is to check if the user has added names to the game.*/
            if (playerName == true)
            {
                nameX = txtXPlayer.Text;
                nameX = nameX.ToUpper();
                nameO = txtOPlayer.Text;
                nameO = nameO.ToUpper();
                /*This is to have the correct user name show up when it is their turn on the label on the form.*/
                if (whosTurn == true)
                {
                    lblWhosTurn.Text = nameX + " Turn";
                    whosTurn = false;
                }
                else if (whosTurn == false)
                {
                    lblWhosTurn.Text = nameO + " Turn";
                    whosTurn = true;
                }
            }
            else
            {
                /*This if statement is to check whos turn it is if no names have been entered into the text boxes of the form.*/
                if (whosTurn == true)
                {
                    lblWhosTurn.Text = "Player X's Turn";
                    whosTurn = false;
                }
                else if (whosTurn == false)
                {
                    lblWhosTurn.Text = "Player O's Turn";
                    whosTurn = true;
                }
            }
            
        }
        /// <summary>
        /// This method is only used to check if the user has enterd names and submited them for the game.
        /// </summary>
        void playerNames()
        {
            /*This if statemnet is checking if the user has entered anynames in the txtboxes and is called once the user
              hits the submit button*/
            if (txtXPlayer.Text != "" && txtOPlayer.Text != "")
            {
                playerName = true;
            }
            else
            {
                playerName = false;
            }
        }

        
    }
}
