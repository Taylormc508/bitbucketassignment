﻿namespace TMcVittieAssignment3
{
    partial class TicTac
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TicTac));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pnlGameBoard = new System.Windows.Forms.Panel();
            this.picb9 = new System.Windows.Forms.PictureBox();
            this.picb8 = new System.Windows.Forms.PictureBox();
            this.picb7 = new System.Windows.Forms.PictureBox();
            this.picb6 = new System.Windows.Forms.PictureBox();
            this.picb5 = new System.Windows.Forms.PictureBox();
            this.picb4 = new System.Windows.Forms.PictureBox();
            this.picb3 = new System.Windows.Forms.PictureBox();
            this.picb2 = new System.Windows.Forms.PictureBox();
            this.picb1 = new System.Windows.Forms.PictureBox();
            this.lblWhosTurn = new System.Windows.Forms.Label();
            this.txtXPlayer = new System.Windows.Forms.TextBox();
            this.txtOPlayer = new System.Windows.Forms.TextBox();
            this.lblPlayerX = new System.Windows.Forms.Label();
            this.lblPlayerO = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnlGameBoard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picb9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "O-Image.jpg");
            this.imageList1.Images.SetKeyName(1, "X-Image.jpg");
            // 
            // pnlGameBoard
            // 
            this.pnlGameBoard.BackColor = System.Drawing.Color.Goldenrod;
            this.pnlGameBoard.Controls.Add(this.picb9);
            this.pnlGameBoard.Controls.Add(this.picb8);
            this.pnlGameBoard.Controls.Add(this.picb7);
            this.pnlGameBoard.Controls.Add(this.picb6);
            this.pnlGameBoard.Controls.Add(this.picb5);
            this.pnlGameBoard.Controls.Add(this.picb4);
            this.pnlGameBoard.Controls.Add(this.picb3);
            this.pnlGameBoard.Controls.Add(this.picb2);
            this.pnlGameBoard.Controls.Add(this.picb1);
            this.pnlGameBoard.Location = new System.Drawing.Point(414, 145);
            this.pnlGameBoard.Name = "pnlGameBoard";
            this.pnlGameBoard.Size = new System.Drawing.Size(439, 396);
            this.pnlGameBoard.TabIndex = 9;
            // 
            // picb9
            // 
            this.picb9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb9.Location = new System.Drawing.Point(289, 260);
            this.picb9.Name = "picb9";
            this.picb9.Size = new System.Drawing.Size(126, 111);
            this.picb9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb9.TabIndex = 8;
            this.picb9.TabStop = false;
            this.picb9.Click += new System.EventHandler(this.picb9_Click);
            // 
            // picb8
            // 
            this.picb8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb8.Location = new System.Drawing.Point(157, 260);
            this.picb8.Name = "picb8";
            this.picb8.Size = new System.Drawing.Size(126, 111);
            this.picb8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb8.TabIndex = 7;
            this.picb8.TabStop = false;
            this.picb8.Click += new System.EventHandler(this.picb8_Click);
            // 
            // picb7
            // 
            this.picb7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb7.Location = new System.Drawing.Point(25, 260);
            this.picb7.Name = "picb7";
            this.picb7.Size = new System.Drawing.Size(126, 111);
            this.picb7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb7.TabIndex = 6;
            this.picb7.TabStop = false;
            this.picb7.Click += new System.EventHandler(this.picb7_Click);
            // 
            // picb6
            // 
            this.picb6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb6.Location = new System.Drawing.Point(289, 142);
            this.picb6.Name = "picb6";
            this.picb6.Size = new System.Drawing.Size(126, 111);
            this.picb6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb6.TabIndex = 5;
            this.picb6.TabStop = false;
            this.picb6.Click += new System.EventHandler(this.picb6_Click);
            // 
            // picb5
            // 
            this.picb5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb5.Location = new System.Drawing.Point(157, 142);
            this.picb5.Name = "picb5";
            this.picb5.Size = new System.Drawing.Size(126, 111);
            this.picb5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb5.TabIndex = 4;
            this.picb5.TabStop = false;
            this.picb5.Click += new System.EventHandler(this.picb5_Click);
            // 
            // picb4
            // 
            this.picb4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb4.Location = new System.Drawing.Point(25, 142);
            this.picb4.Name = "picb4";
            this.picb4.Size = new System.Drawing.Size(126, 111);
            this.picb4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb4.TabIndex = 3;
            this.picb4.TabStop = false;
            this.picb4.Click += new System.EventHandler(this.picb4_Click);
            // 
            // picb3
            // 
            this.picb3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb3.Location = new System.Drawing.Point(289, 25);
            this.picb3.Name = "picb3";
            this.picb3.Size = new System.Drawing.Size(126, 111);
            this.picb3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb3.TabIndex = 2;
            this.picb3.TabStop = false;
            this.picb3.Click += new System.EventHandler(this.picb3_Click);
            // 
            // picb2
            // 
            this.picb2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb2.Location = new System.Drawing.Point(157, 25);
            this.picb2.Name = "picb2";
            this.picb2.Size = new System.Drawing.Size(126, 111);
            this.picb2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb2.TabIndex = 1;
            this.picb2.TabStop = false;
            this.picb2.Click += new System.EventHandler(this.picb2_Click);
            // 
            // picb1
            // 
            this.picb1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.picb1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picb1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picb1.Location = new System.Drawing.Point(25, 25);
            this.picb1.Name = "picb1";
            this.picb1.Size = new System.Drawing.Size(126, 111);
            this.picb1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picb1.TabIndex = 0;
            this.picb1.TabStop = false;
            this.picb1.Click += new System.EventHandler(this.picb1_Click);
            // 
            // lblWhosTurn
            // 
            this.lblWhosTurn.AutoSize = true;
            this.lblWhosTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWhosTurn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblWhosTurn.Location = new System.Drawing.Point(26, 185);
            this.lblWhosTurn.Name = "lblWhosTurn";
            this.lblWhosTurn.Size = new System.Drawing.Size(0, 37);
            this.lblWhosTurn.TabIndex = 10;
            // 
            // txtXPlayer
            // 
            this.txtXPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXPlayer.Location = new System.Drawing.Point(245, 377);
            this.txtXPlayer.Name = "txtXPlayer";
            this.txtXPlayer.Size = new System.Drawing.Size(101, 26);
            this.txtXPlayer.TabIndex = 11;
            // 
            // txtOPlayer
            // 
            this.txtOPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOPlayer.Location = new System.Drawing.Point(246, 420);
            this.txtOPlayer.Name = "txtOPlayer";
            this.txtOPlayer.Size = new System.Drawing.Size(100, 26);
            this.txtOPlayer.TabIndex = 12;
            // 
            // lblPlayerX
            // 
            this.lblPlayerX.AutoSize = true;
            this.lblPlayerX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerX.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblPlayerX.Location = new System.Drawing.Point(115, 380);
            this.lblPlayerX.Name = "lblPlayerX";
            this.lblPlayerX.Size = new System.Drawing.Size(124, 20);
            this.lblPlayerX.TabIndex = 13;
            this.lblPlayerX.Text = "Player X\'s Name";
            // 
            // lblPlayerO
            // 
            this.lblPlayerO.AutoSize = true;
            this.lblPlayerO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerO.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblPlayerO.Location = new System.Drawing.Point(115, 423);
            this.lblPlayerO.Name = "lblPlayerO";
            this.lblPlayerO.Size = new System.Drawing.Size(125, 20);
            this.lblPlayerO.TabIndex = 14;
            this.lblPlayerO.Text = "Player O\'s Name";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblMessage.Location = new System.Drawing.Point(85, 346);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(273, 24);
            this.lblMessage.TabIndex = 15;
            this.lblMessage.Text = "Player Names are not Required";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(245, 465);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 16;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Showcard Gothic", 72F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblTitle.Location = new System.Drawing.Point(134, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(606, 119);
            this.lblTitle.TabIndex = 17;
            this.lblTitle.Text = "Tic Tac Toe";
            // 
            // TicTac
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.ClientSize = new System.Drawing.Size(884, 553);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblPlayerO);
            this.Controls.Add(this.lblPlayerX);
            this.Controls.Add(this.txtOPlayer);
            this.Controls.Add(this.txtXPlayer);
            this.Controls.Add(this.lblWhosTurn);
            this.Controls.Add(this.pnlGameBoard);
            this.Name = "TicTac";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.TicTac_Load);
            this.pnlGameBoard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picb9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picb1;
        private System.Windows.Forms.PictureBox picb2;
        private System.Windows.Forms.PictureBox picb3;
        private System.Windows.Forms.PictureBox picb4;
        private System.Windows.Forms.PictureBox picb5;
        private System.Windows.Forms.PictureBox picb6;
        private System.Windows.Forms.PictureBox picb7;
        private System.Windows.Forms.PictureBox picb8;
        private System.Windows.Forms.PictureBox picb9;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel pnlGameBoard;
        private System.Windows.Forms.Label lblWhosTurn;
        private System.Windows.Forms.TextBox txtXPlayer;
        private System.Windows.Forms.TextBox txtOPlayer;
        private System.Windows.Forms.Label lblPlayerX;
        private System.Windows.Forms.Label lblPlayerO;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lblTitle;
    }
}

